#include <Server.h>

MyServer::MyServer()
{
    if(server.listen(QHostAddress::Any, 5555))
    {
        qDebug() << "Listening";
        qDebug() << "Waiting for new clients";
    }
    else
    {
        qDebug() << "Not listening";
    }

    database = QSqlDatabase::addDatabase("QSQLITE");
    database.setDatabaseName("./database.db");
    query = new QSqlQuery(database);

    if(database.open())
    {
        qDebug() << "Database is opened";
    }
    else
    {
         qDebug() << "Database isn't opened";
    }
    DropOldAndCreateNewDatabase();

    connect(&server, SIGNAL(newConnection()), this, SLOT(onNewConnection()));
}

MyServer::~MyServer()
{
    server.close();

    for(auto& socket : sockets)
    {
        sockets.removeOne(socket);
        socket->deleteLater();
    }
}

QString MyServer::MakeJsonForDays()
{
    QString result = "{\"timetable\":\"new\"";

    for(auto it = TimeTable.begin(); it != TimeTable.end(); it++)
    {
         if(it == TimeTable.begin())
         {
             result += ",";
         }

         result += "\"" + it.key() + "\":[";

         for(auto v = it.value().begin(); v != it.value().end(); v++)
         {
             result += QString::number(*v);

             if(v != std::prev(it.value().end()))
             {
                 result += ",";
             }
         }
         result += "]";

         if(it != std::prev(TimeTable.end()))
         {
             result += ",";
         }

    }
    result += "}\n";
    return result;
}

void MyServer::InsertDataIntoDatabase(const QJsonObject &obj)
{
    QString name = obj.value("name").toString();
    QString surname = obj.value("surname").toString();
    QString patronym = obj.value("patronym").toString();

    QString address = obj.value("address").toString();
    QString phone_number = obj.value("phone_number").toString();
    QString birthday = obj.value("birthday").toString();

    QString data_of_favor = obj.value("data of favor").toString();
    QString time_of_favor = obj.value("time of favor").toString();
    QString payment = obj.value("payment").toString();

    QString favor;
    if(obj.value("favor").toString() == "0")
    {
        favor = "Passport of a citizen of Ukraine in the form of a card";
    }
    else
    {
        favor = "Passport of a citizen of Ukraine for travel abroad";
    }

    QString string_query = "INSERT INTO InfoAboutClients(FirstName, LastName, Patronym, DataOfFavor, TimeOfFavor,"
           "Favor, Address, PhoneNumber, DataOfBithday, Payment)"
           "VALUES(\"" + name + "\", \"" + surname + "\", \"" + patronym + "\", \""
            + data_of_favor + "\", \"" + time_of_favor + "\", \"" + favor + "\", \""
            + address + "\", \"" + phone_number + "\", \"" + birthday + "\", \"" + payment  + "\");";

    if(query->exec(string_query))
    {
        qDebug() << "The query for database was executed (INSERT DATA INTO InfoAboutClients)";
    }
    else
    {
        qDebug() << "The query for database wasn't executed (INSERT DATA INTO InfoAboutClients)";
    }
}

void MyServer::DropOldAndCreateNewDatabase()
{
    if(query->exec("DROP TABLE IF EXISTS InfoAboutClients;"))
    {
        qDebug() << "The query for database was executed (Drop table if exists InfoAboutClients)";
    }
    else
    {
        qDebug() << "The query for database wasn't executed (Drop table if exists InfoAboutClients)";
    }


    if(query->exec("CREATE TABLE IF NOT EXISTS InfoAboutClients(ID INTEGER PRIMARY KEY AUTOINCREMENT, FirstName TEXT, LastName TEXT, Patronym TEXT, "
                   "DataOfFavor DATE, TimeOfFavor INTEGER, Favor TEXT, Address TEXT, PhoneNumber TEXT, DataOfBithday DATE, Payment TEXT);"))
    {
         qDebug() << "The query for database was executed (Create table if not exists InfoAboutClients)";
    }
    else
    {
         qDebug() << "The query for database wasn't executed (Create table if not exists InfoAboutClients)";
    }
}

void MyServer::onNewConnection()
{
   QTcpSocket *clientSocket = server.nextPendingConnection();
   connect(clientSocket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
   connect(clientSocket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(onSocketStateChanged(QAbstractSocket::SocketState)));

   qDebug() << "-----------------------------------------------------------";
   qDebug() << "Client" << clientSocket->socketDescriptor() << "connected";
   qDebug() << "-----------------------------------------------------------";

   QString infoAboutDays = MakeJsonForDays();
   qDebug() << "Client" << clientSocket->socketDescriptor()<< "got new timetable";
   clientSocket->write(infoAboutDays.toStdString().c_str());

   sockets.push_back(clientSocket);
   socketDescriptors.insert(clientSocket->socketDescriptor());
}

void MyServer::onSocketStateChanged(QAbstractSocket::SocketState socketState)
{
    if (socketState == QAbstractSocket::UnconnectedState)
    {
        QTcpSocket* sender = static_cast<QTcpSocket*>(QObject::sender());
        sockets.removeOne(sender);
        sender->deleteLater();

        std::set<int> actualSockets;

        for(const auto& iter : sockets)
        {
            actualSockets.insert(iter->socketDescriptor());
        }

        std::set<int> result;
        std::set_difference(socketDescriptors.begin(), socketDescriptors.end(), actualSockets.begin(), actualSockets.end(),
            std::inserter(result, result.end()));

        for(const auto& iter : result)
        {
            qDebug() << "Client" << iter << "disconnected";
            socketDescriptors.erase(iter);
        }
    }
}

void MyServer::onReadyRead()
{
    QTcpSocket* sender = static_cast<QTcpSocket*>(QObject::sender());
    sender->waitForReadyRead(1000);

    QByteArray message = sender->readAll();
    QJsonParseError docErrors;
    QJsonDocument doc = QJsonDocument::fromJson(message, &docErrors);

    if (docErrors.errorString()== "no error occurred")
    {
        if(doc.object().value("request").toString() == "new registration")
        {
            QString date = doc.object().value("data of favor").toString();
            int time = doc.object().value("time of favor").toString().toInt();

            qDebug() << "Client" << sender->socketDescriptor()
            << "sent request: new registration";

            QString answer;

            if(TimeTable[date].count(time))
            {
                qDebug() << "Client" << sender->socketDescriptor()
                << "got answer for request: new registration - No";

                answer = "{\"result_reg\":\"No\",\"payment\":\"" + doc.object().value("payment").toString() +"\"}\n";
                sender->write(answer.toStdString().c_str());
            }
            else
            {
                qDebug() << "Client" << sender->socketDescriptor()
                << "got answer for request: new registration - Yes";

                InsertDataIntoDatabase(doc.object());

                answer = "{\"result_reg\":\"Yes\",\"payment\":\"" + doc.object().value("payment").toString() +"\"}\n";
                sender->write(answer.toStdString().c_str());
                TimeTable[date].insert(time);

                QString infoAboutDays = MakeJsonForDays();

                for(const auto& socket : sockets)
                {
                    qDebug() << "Client" << socket->socketDescriptor()<< "got new timetable";

                    socket->write(infoAboutDays.toStdString().c_str());
                    socket->waitForBytesWritten(1000);
                }
            }
        }
        else
        {
             qDebug() << "Client" << sender->socketDescriptor()
             << "sent unknown request: " << doc.object().value("request").toString();
        }
    }
    else
    {
        qDebug() << "JSON error: " << docErrors.errorString();
    }
}

