#ifndef SERVER_H
#define SERVER_H

#include <QTcpServer>
#include <QTcpSocket>
#include <QDebug>
#include <QHostAddress>
#include <QAbstractSocket>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>
#include <set>

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>

class MyServer : public QObject
{
    Q_OBJECT
public:
    MyServer();
    ~MyServer();
    QString MakeJsonForDays();
    void InsertDataIntoDatabase(const QJsonObject& obj);
    void DropOldAndCreateNewDatabase();

public slots:
    void onNewConnection();
    void onSocketStateChanged(QAbstractSocket::SocketState socketState);
    void onReadyRead();

private:
    QTcpServer server;
    QList<QTcpSocket*> sockets;
    std::set<int> socketDescriptors;

    QMap<QString, std::set<int>> TimeTable;

    QSqlDatabase database;
    QSqlQuery* query;
};

#endif


