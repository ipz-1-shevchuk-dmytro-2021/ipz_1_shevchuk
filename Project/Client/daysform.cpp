#include "daysform.h"
#include "ui_daysform.h"

DaysForm::DaysForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DaysForm)
{
    ui->setupUi(this);
    setFixedSize(geometry().width(),geometry().height());
    ui->dateEdit_Data->setMinimumDate(QDate::currentDate());
    ui->dateEdit_Data->setMaximumDate(QDate::currentDate().addDays(150));
}

DaysForm::~DaysForm()
{
    delete ui;
}

void DaysForm::on_pushButton_Exit_clicked()
{
    ui->listWidget_Hours->clear();
    close();
}

void DaysForm::ReceiveTimeTable(QMap<QString, std::set<int>> NewTimeTable)
{
    TimeTable = NewTimeTable;
}

void DaysForm::on_pushButton_Find_clicked()
{
     QString data = ui->dateEdit_Data->date().toString("dd-MM-yyyy");
     ui->listWidget_Hours->clear();

     if(TimeTable.count(data))
     {
         std::set<int> all_hours = {8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
         std::set<int> busy_hours = TimeTable[data];

         std::set<int> result;
         std::set_difference(all_hours.begin(), all_hours.end(), busy_hours.begin(), busy_hours.end(),
             std::inserter(result, result.end()));

         if(result.size() > 0)
         {
             for(const auto& value : result)
             {
                 ui->listWidget_Hours->addItem(QString::number(value) + ":00");
             }
         }
         else
         {
            ui->listWidget_Hours->addItem("Вільних годин нажаль немає");
         }
     }
     else
     {
         ui->listWidget_Hours->addItems({"8:00", "9:00", "10:00", "11:00", "12:00",
         "13:00", "14:00", "15:00", "16:00", "17:00", "18:00"});
     }
}

