#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>
#include <QJsonArray>

#include "fillform.h"
#include "daysform.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QTcpSocket* socket;
    QByteArray data;

public slots:
    void sockReady();
    void sockDisc();
    void ReciveDataFromFillForm(QString message);

private slots:
    void on_pushButton_fillForm_clicked();
    void on_pushButton_exit_clicked();
    void on_pushButton_showDays_clicked();

signals:
    void SendTimeTable(QMap<QString, std::set<int>>);

private:
    Ui::MainWindow *ui;
    FillForm* fillForm;
    DaysForm* daysForm;
};
#endif // MAINWINDOW_H
