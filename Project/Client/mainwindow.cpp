#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    fillForm = new FillForm();
    daysForm = new DaysForm();

    connect(fillForm, &FillForm::SendDataToMainForm, this, &MainWindow::ReciveDataFromFillForm);
    connect(this, &MainWindow::SendTimeTable, daysForm, &DaysForm::ReceiveTimeTable);

    socket = new QTcpSocket(this);
    connect(socket, SIGNAL(readyRead()), this, SLOT(sockReady()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(sockDisc()));

    socket->connectToHost("127.0.0.1", 5555);

    if(!socket->waitForConnected(2000))
    {
       QMessageBox::critical(this, "Критична помилка", "Немає з'єднання з сервером!!");
       exit(1);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::sockDisc()
{
    QMessageBox::critical(this, "Критична помилка", "З'єднання з сервером було втрачене!!");
    socket->deleteLater();
    exit(1);
}

void MainWindow::sockReady()
{
    while(socket->canReadLine())
    {
        if(socket->waitForConnected(500))
        {
            socket->waitForReadyRead(500);

            QByteArray message = socket->readLine();

            QJsonParseError docErrors;
            QJsonDocument doc = QJsonDocument::fromJson(message, &docErrors);

            if (docErrors.errorString()== "no error occurred")
            {
                if(doc.object().value("result_reg").toString() == "Yes" && doc.object().count() == 2)
                {
                    QMessageBox msgAbout;
                    QString message = "<p style='text-align: center;'><img src=':/image/images/happy_smile.png' alt='' width='100' height='100'></p>"
                                            "<p style='text-align: center;'><strong>Ви успішно зареєструвались на дану послугу</strong></p>"
                                            "<p style='text-align: center;'><strong>Перелік необхідних документів для отримання послуги:</strong></p>"
                                            "<p style='text-align: center;'>&nbsp;</p>"
                                            "<p>1) Паспорт громадянина України</p>"
                                            "<p>2) Індивідуальний податковий номер фізичної особи — платника податків (ідентифікаційний номер)</p>"
                                            "<p>3) Довідка про реєстрацію місця проживання (за наявності)</p>"
                                            "<p style='text-align: center;'>&nbsp;</p>";

                    if(doc.object().value("payment").toString() == "Yes")
                    {
                        message += "<p style='text-align: center;'>Послуга була успішно оплачена</p>";
                    }

                    msgAbout.setWindowTitle("Результат реєстрації");
                    msgAbout.setWindowIcon(QPixmap(":/image/images/cliente.png"));
                    msgAbout.setText(message);
                    msgAbout.exec();
                }
                else if(doc.object().value("result_reg").toString() == "No" && doc.object().count() == 2)
                {
                    QMessageBox msgAbout;
                    QString message = "<p style='text-align: center;'><img src=':/image/images/sad_smile.png' alt='' width='100' height='100'></p>"
                                            "<p style='text-align: center;'><strong>Нажаль, не вдалось зареєструватись на дану послугу, обрана дата занята</strong></p>"
                                            "<p style='text-align: center;'><strong>Перегляньте, будь ласка, список вільних годин</strong></p>";

                    if(doc.object().value("payment").toString() == "Yes")
                    {
                        message += "<p style='text-align: center;'>Послуга не була оплачена</p>";
                    }

                    msgAbout.setWindowTitle("Результат реєстрації");
                    msgAbout.setWindowIcon(QPixmap(":/image/images/cliente.png"));
                    msgAbout.setText(message);
                    msgAbout.exec();
                }
                else if(doc.object().value("timetable").toString() == "new")
                {
                    QMap<QString, std::set<int>> TimeTable;

                    foreach(const QString& key, doc.object().keys())
                    {
                        if(key != "timetable")
                        {
                            QJsonArray times = doc.object().value(key).toArray();

                            for(int i=0; i<times.count();i++)
                            {
                                TimeTable[key].insert(times[i].toInt());
                            }
                        }
                    }

                    emit SendTimeTable(TimeTable);

                    for(auto it = TimeTable.begin(); it != TimeTable.end(); it++)
                    {
                        qDebug() << it.key();
                        for(auto value = it.value().begin(); value != it.value().end(); value++)
                        {
                             qDebug() << *value;
                        }
                        qDebug() << "\n";
                    }

                }

            }
            else
            {
                 qDebug() << "JSON error: " << docErrors.errorString();
                 QMessageBox::critical(this, "Критична помилка", "Виникли помилки при отриманні даних");
            }
        }
    }


}

void MainWindow::on_pushButton_fillForm_clicked()
{
     hide();
     fillForm->exec();
     show();
}

void MainWindow::on_pushButton_showDays_clicked()
{
    hide();
    daysForm->exec();
    show();
}

void MainWindow::on_pushButton_exit_clicked()
{
    QMessageBox::StandardButton reply =
            QMessageBox::question(this, "Підтвердження виходу з програми", "Ви дійсно хочете вийти з програми?", QMessageBox::Yes | QMessageBox::No);

    if(reply == QMessageBox::Yes)
    {
        QApplication::quit();
    }
}

void MainWindow::ReciveDataFromFillForm(QString message)
{
    if(socket->isOpen())
    {
        socket->write(message.toStdString().c_str());
        socket->waitForBytesWritten(500);
    }
    else
    {
        QMessageBox::warning(this, "Помилка", "Ваші дані нажаль не відправились на сервер, спробуйте ще раз");
    }
}

