#include "fillform.h"
#include "ui_fillform.h"

FillForm::FillForm(QWidget *parent) : QDialog(parent), ui(new Ui::FillForm)
{
    ui->setupUi(this);

    connect(ui->lineEdit_Name, SIGNAL(textChanged(QString)), this, SLOT(EnableButtonPay()));
    connect(ui->lineEdit_Surname, SIGNAL(textChanged(QString)), this, SLOT(EnableButtonPay()));
    connect(ui->lineEdit_Patronym, SIGNAL(textChanged(QString)), this, SLOT(EnableButtonPay()));
    connect(ui->lineEdit_Phonenumber, SIGNAL(textChanged(QString)), this, SLOT(EnableButtonPay()));
    connect(ui->lineEdit_Address, SIGNAL(textChanged(QString)), this, SLOT(EnableButtonPay()));

    QPixmap pix(":/image/images/fill_form.png");
    ui->FillFormImage->setPixmap(pix.scaled(ui->FillFormImage->minimumWidth(),ui->FillFormImage->minimumHeight(), Qt::KeepAspectRatio));
    ui->dateEdit_Calendar->setMinimumDate(QDate::currentDate());
    ui->dateEdit_Calendar->setMaximumDate(QDate::currentDate().addDays(150));


    payForm = new PayForm();

    ui->lineEdit_Phonenumber->setValidator(new QRegularExpressionValidator(QRegularExpression("^[0-9]{10}$")));
    ui->lineEdit_Name->setValidator(new QRegularExpressionValidator(QRegularExpression("[a-zA-Z]{3,}")));
    ui->lineEdit_Surname->setValidator(new QRegularExpressionValidator(QRegularExpression("[a-zA-Z]{3,}")));
    ui->lineEdit_Patronym->setValidator(new QRegularExpressionValidator(QRegularExpression("[a-zA-Z]{3,}")));


    ui->pushButton_Buy->setEnabled(false);
    ui->pushButton_Confirm->setEnabled(false);

    connect(payForm, &PayForm::QuitToMainForm, this, &FillForm::on_pushButton_Exit_clicked);
    connect(payForm, &PayForm::QuitToFillForm, this, &FillForm::ContinueRegistration);
}

FillForm::~FillForm()
{
    delete ui;
}

void FillForm::on_pushButton_Exit_clicked()
{
    ClearForm();
    close();
}

void FillForm::on_pushButton_Confirm_clicked()
{
    QString result = GetDataFromForm() + "\"payment\":\"No\"}";

    QMessageBox::information(this, "Результат збереження даних", "Дані успішно збережено!\t");
    emit SendDataToMainForm(result);
    ClearForm();
}

void FillForm::on_pushButton_Buy_clicked()
{
    payForm->exec();
}

void FillForm::EnableButtonPay()
{
    if(ui->lineEdit_Name->text() != "" && ui->lineEdit_Surname->text() != "" && ui->lineEdit_Patronym->text() != ""
          && ui->lineEdit_Phonenumber->hasAcceptableInput() && ui->lineEdit_Address->text() != "")
    {
        ui->pushButton_Buy->setEnabled(true);
        ui->pushButton_Confirm->setEnabled(true);
    }
    else
    {
        ui->pushButton_Buy->setEnabled(false);
        ui->pushButton_Confirm->setEnabled(false);
    }
}

void FillForm::ContinueRegistration()
{
    QString result = GetDataFromForm() + "\"payment\":\"Yes\"}";

    emit SendDataToMainForm(result);
    ClearForm();
}

void FillForm::ClearForm()
{
    ui->lineEdit_Name->clear();
    ui->lineEdit_Surname->clear();
    ui->lineEdit_Patronym->clear();
    ui->lineEdit_Address->clear();
    ui->lineEdit_Phonenumber->clear();

    ui->dateEdit_Birthday->setDate(ui->dateEdit_Birthday->maximumDate());
    ui->dateEdit_Calendar->setDate(ui->dateEdit_Calendar->minimumDate());
}

QString FillForm::GetDataFromForm()
{
    QString result;
    result += "{\"request\":\"new registration\",";
    result += "\"name\":\"" + ui->lineEdit_Name->text() + "\",";
    result += "\"surname\":\"" + ui->lineEdit_Surname->text() + "\",";
    result += "\"patronym\":\"" + ui->lineEdit_Patronym->text() + "\",";
    result += "\"address\":\"" + ui->lineEdit_Address->text() + "\",";
    result += "\"phone_number\":\"" + ui->lineEdit_Phonenumber->text() + "\",";
    result += "\"birthday\":\"" + ui->dateEdit_Birthday->date().toString("dd-MM-yyyy") + "\",";

    result += "\"data of favor\":\"" + ui->dateEdit_Calendar->date().toString("dd-MM-yyyy") + "\",";
    result += "\"time of favor\":\"" + QString::number(ui->spinBox_Time->value()) + "\",";
    result += "\"favor\":\"" + QString::number(ui->comboBox->currentIndex()) + "\",";

    return result;
}



