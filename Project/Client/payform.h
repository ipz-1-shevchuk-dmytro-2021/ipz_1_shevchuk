#ifndef PAYFORM_H
#define PAYFORM_H

#include <QDialog>
#include <QRegularExpressionValidator>
#include <QDate>
#include <QMessageBox>
#include <QDebug>

namespace Ui {
class PayForm;
}

class PayForm : public QDialog
{
    Q_OBJECT

public:
    explicit PayForm(QWidget *parent = nullptr);
    void ClearForm();
    ~PayForm();

private slots:
    void on_pushButton_Pay_clicked();
    void EnableButton();
    void on_pushButton_Exit_clicked();

signals:
    void QuitToMainForm();
    void QuitToFillForm();

private:
    Ui::PayForm *ui;


};

#endif // PAYFORM_H
