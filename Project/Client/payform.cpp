#include "payform.h"
#include "ui_payform.h"

PayForm::PayForm(QWidget *parent) : QDialog(parent), ui(new Ui::PayForm)
{
    ui->setupUi(this);
    setFixedSize(geometry().width(),geometry().height());

    connect(ui->lineEdit_Month, SIGNAL(textChanged(QString)), this, SLOT(EnableButton()));
    connect(ui->lineEdit_Year, SIGNAL(textChanged(QString)), this, SLOT(EnableButton()));
    connect(ui->lineEdit_NumCard, SIGNAL(textChanged(QString)), this, SLOT(EnableButton()));
    connect(ui->lineEdit_CVV, SIGNAL(textChanged(QString)), this, SLOT(EnableButton()));

    ui->lineEdit_NumCard->setValidator(new QRegularExpressionValidator(QRegularExpression("^[0-9]{16}$")));
    ui->lineEdit_Month->setValidator(new QIntValidator(1, 12,this));

    int current_year = QDate::currentDate().year();
    ui->lineEdit_Year->setValidator(new QIntValidator(current_year, current_year + 10));
    ui->lineEdit_CVV->setValidator(new QRegularExpressionValidator(QRegularExpression("^[0-9]{3}$")));
    ui->pushButton_Pay->setEnabled(false);
}

void PayForm::ClearForm()
{
    ui->lineEdit_NumCard->clear();
    ui->lineEdit_Month->clear();
    ui->lineEdit_Year->clear();
    ui->lineEdit_CVV->clear();
}

PayForm::~PayForm()
{
    delete ui;
}

void PayForm::on_pushButton_Pay_clicked()
{
   QMessageBox::information(this, "Результат збереження даних", "Дані успішно збережено!\t");
   ClearForm();
   close();
   emit QuitToFillForm();
}

void PayForm::EnableButton()
{
    int month = ui->lineEdit_Month->text().toInt();
    int year = ui->lineEdit_Year->text().toInt();

    if(ui->lineEdit_Year->hasAcceptableInput() && ui->lineEdit_Month->hasAcceptableInput()
            && ui->lineEdit_CVV->hasAcceptableInput() && ui->lineEdit_NumCard->hasAcceptableInput()
        && ((year == QDate::currentDate().year() && month >= QDate::currentDate().month())
            || (year > QDate::currentDate().year())))
    {
        ui->pushButton_Pay->setEnabled(true);
    }
    else
    {
        ui->pushButton_Pay->setEnabled(false);
    }
}

void PayForm::on_pushButton_Exit_clicked()
{
    ClearForm();
    close();
    emit QuitToMainForm();
}


