#ifndef FILLFORM_H
#define FILLFORM_H

#include <QDialog>
#include "payform.h"

namespace Ui {
class FillForm;
}

class FillForm : public QDialog
{
    Q_OBJECT

public:
    explicit FillForm(QWidget *parent = nullptr);
    ~FillForm();
    void ClearForm();
    QString GetDataFromForm();

private slots:
    void on_pushButton_Exit_clicked();
    void on_pushButton_Confirm_clicked();
    void on_pushButton_Buy_clicked();
    void EnableButtonPay();
    void ContinueRegistration();


signals:
    void SendDataToMainForm(QString);

private:
    Ui::FillForm *ui;
    PayForm* payForm;
};

#endif // FILLFORM_H
