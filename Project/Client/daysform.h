#ifndef DAYSFORM_H
#define DAYSFORM_H

#include <QDialog>
#include <QMap>
#include <set>

namespace Ui {
class DaysForm;
}

class DaysForm : public QDialog
{
    Q_OBJECT

public:
    explicit DaysForm(QWidget *parent = nullptr);
    ~DaysForm();

private slots:
    void on_pushButton_Exit_clicked();

    void on_pushButton_Find_clicked();

public slots:
    void ReceiveTimeTable(QMap<QString, std::set<int>> NewTimeTable);

private:
    Ui::DaysForm *ui;
    QMap<QString, std::set<int>> TimeTable;

};

#endif // DAYSFORM_H
